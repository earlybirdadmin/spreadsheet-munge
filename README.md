**Setup**

* To run these scripts you'll need to install R and a few packages with
    `install.packages("openxlsx", "forcats", "googlesheets", "bindrcpp","dplyr", "purrr", "readr",

 "tidyr", "tibble", "ggplot2", "tidyverse", "lubridate", "stringr", "hash", "trelloR")`
* Recommended that you install RStudio IDE as well but any of these scripts can be run from the command line
    * This RProject's working directory is always the top level directory even if you're running a script in a sub-directory


**Directories**

* Top-level directory houses list of current projects and their names and the morning report scripts

* `munge` houses all munging files
* `helpers` has customer helper functions used in scripts in both `munge` and `report`
* `report` has scripts that source in scripts used in `munge`
* `kroski` has a tiny Shiny app of all the points Kris has done this week
* `misc` is miscellaneous and/or unfinished things


**Reference**

* `current_projects.R` contains up-to-date vectors of
    * Original Trello board names to pull in in `project_names_initial`
    * Names of projects post-collapsing and lowercasing in `projects_in_hours`
    * Final project names that will replace the original Trello names in `project_names_final`
    * `projects_in_hours` and `project_names_final` are associated in `proj_name_dict`

**Munging**

Munging is done before any of the report scripts are run.
`api.R` munges points and `hours.R` munges hours; they are merged in `combine.R` which sources both of them. 

* `api.R` 

    * Grabs all tickets from all the Trello boards we want
       * Note that `trello_keys.R` is gitignored
    * Extracts corresponding list names and labels from their IDs
    * Adds up to 4 members assigned to a ticket from their IDs
        * Change names to match what we'll have at the end of hours munging, so the `new_names` in `dedupe_names.R` which is read in in `munge_hours.R`
    * Extracts points and consumed points from the ticket title
    * Infers end of week date, week number, and year that a ticket was done on from the due date
        * From this we label ticket as done (Yes/No), delivered (Yes/No)
    * Applies billing logic, e.g., all Fletcher points are billed, UniB use it or lose it, etc.
    * Looks for contractors

* `munge_hours.R`

    * Reads in a CSV of the old Hours googlesheet
    * Reads in the new Hours googlesheet 
    * Merges the two
    * Takes all project names: removes spaces, makes them lowercase, and combines names that refer to the same project (e.g., "pennsy" and "bosyn")
        * What we're left with should match `projects_in_hours` from `current_projects.R`, but just in case, filter down to just those projects
    * Dedupe people
        * Source in `dedupe_names.R` so we're left with names 
    * Dedupe rows, if the exact same rows appear in the old and new Hours logs
    * Get hours per week per project
    * Get hours per week per person project
    * Join on a dataframe of all weeks and years from 2015-2017 (read in in `weeks_and_year.R`)

* `combine.R`

    * Find points done per week (since hours are on a weekly not daily basis)
    * Merge points and hours done per week
    * Get hours per week per person

 
**Reporting**

Scripts that only need to report points only source in `api.R`. For reports that also need hours, we source in `combine.R`.

* `morning_report.R`

	 * *Input*: sources in `api.R` 
	 * *Output*: POSTS up the points done yesterday, points left in sprint, etc. to three googlesheets, one of which is the Production Standups Log
    * This script kept in the same level directory as the .Rproj working directory rather than in a sub-directory so we can use the same relative file paths to access files in, for instance, the `api` sub-directory
    *  Read in `get_todays_date.R` which by default finds today's date and from there get's yesterday's (if it's Monday yesterday is Friday through Sunday)
    *  Read in the Sprint Dates sheet of the Production Standups Log to get sprint start and end dates
    *  Munge points to get a total and billed for points done yesterday as well as left in the sprint, etc.

* `morning_report_revert.R`
    * Re-do the morning report script for every day in the current week (Monday through today)
        * This is useful if there were retroactive changes to the Trello that need to be reflected on Production Log

* `sprint_report.R`

    * *Input*: sources in `api.R`
    * *Output*: a CSV with different tabs for 
    * Before running this specify:
        * The project name, sprint start and end dates, and columns that will be used to count upcoming sprint tickets
        * The `on_deck_colums` for tickets that will be included in the following sprint
        * `lists_to_exclude` should be pretty comprehensive but may need to be added to: it excludes tickets that are in these lists from counting toward the project Approved Backlog

    * All tickets that are `done` within the date range are counted as completed during this sprint

* `reporting.R`

    * *Input*: sources in `combine.R`
    * *Output*: a dataframe that is the weekly report for all points (billed and unbilled) and hours done per project for the previous week
        * This was the script used for Monday morning meeting point reports and to fill out the Master Revenue and Production Log
    * Sources in `get_eow_date.R` which gets the end of week date of the current week automatically. If you want to change that to a different week, run the function `ask_eow()` in the same script.

* `run_the_jewels.R` runs everything in `reporting.R` 
    
**Other**

* `points_hours_ratio.R` finds points:hours ratios per person, per project, and per person per project

* `weekly_waste.R` calculates 75% utilization 
    * This formula may need to be updated