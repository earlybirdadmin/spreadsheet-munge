

# the main function

make_report <- function(d) {

  # get this particular yesterday
  this_yesterday <- find_yesterday(d)
  all_report <- list()
  
  for (p in standups_log$Project) {
    
    this_project <- p
    start_date <- standups_log[standups_log$Project == p, ]$`Sprint Start Date`
    end_date <- standups_log[standups_log$Project == p, ]$`Sprint End Date`
    
    data_to_use <- data_to_use %>% look_for_contractors()
    
    this_project_tickets <- data_to_use %>% 
      dplyr::filter(
        project == p
      )
    
    this_project_tickets <- this_project_tickets %>%
      select(points, list_name, card_num, card_name, description, 
             labels, Member1, Member2, done,
             billed, wk_num, date, contract) %>%
      rename(
        Points = points,
        `List` = list_name,
        `Ticket Number` = card_num,
        `Ticket Name` = card_name,
        Description = description,
        Labels = labels,
        `Done?` = done,
        `Billed?` = billed,
        `Week of` = wk_num,
        `Date Completed` = date,
        `Contractor?` = contract
      )
    
    # drop list levels that don't exist in our project so that when we're searching for whether an Original Epics column exists
    # in this project we only search for lists in this_project and not all projects
    this_project_tickets <- this_project_tickets %>% droplevels()
    
    
    # set total project points to NA to start with type of double
    # if there's no original_epic_column, take the total points from the standups log. we're saying they're all billed by default.
    # else it comes from the original_epic_column
    find_total_project_points <- function() {
      
      total_project_points <- list("Billed" = NA, "Unbilled" = NA, "Total" = NA) %>% 
        map(., as.numeric) %>% 
        as_tibble() 
      
      if (! original_epic_column %in% this_project_tickets$List) {
        total_project_points$Total <- standups_log[which(standups_log$Project == this_project), 
                                                   which(names(standups_log) == "Total Project Points")]
        
        total_project_points$Billed <- total_project_points$Total
        total_project_points$Unbilled <- 0
        
        total_project_points <- total_project_points %>% unnest()
        
      } else {
        total_project_points <- this_project_tickets %>% 
          filter(
            List == original_epic_column
          ) %>% 
          summarise_points()
      }
      
      return(total_project_points)
    }
    
    total_project_points <- find_total_project_points()
    
    
    
    segment_tickets <- function(sprint_start, sprint_end) {
      
      done_before_this_sprint_tickets <- this_project_tickets %>% 
        filter(
          `Done?` == "Yes"
        ) %>% 
        filter(
          between(`Date Completed`, as.Date("2000-01-01"), as.Date(end_date))
        )
      
      done_this_sprint_tickets <- this_project_tickets %>% 
        filter(
          `Done?` == "Yes"
        ) %>% 
        filter(
          between(`Date Completed`, as.Date(start_date), as.Date(end_date))
        )
      
      # If it's, say, Monday and this_yesterday is Friday, make sure we count points done over the weekend
      if (wday(this_yesterday) == 6) {
        done_yesterday <- this_project_tickets %>% 
          filter(
            `Done?` == "Yes"
          ) %>% 
          filter(
            between(`Date Completed`, this_yesterday, this_yesterday + 2)
          )
      } else {
        done_yesterday <- this_project_tickets %>% 
          filter(
            `Done?` == "Yes"
          ) %>% 
          filter(
            `Date Completed` == this_yesterday
          )
      }
      
      contractor_done_yesterday <- done_yesterday %>% 
        filter(
          `Contractor?` == "Yes"
        )
      
      
      left_this_sprint_tickets <- this_project_tickets %>% 
        filter(
          (List %in% on_deck_columns)
        )
      
      whole_sprint_tickets <- rbind(left_this_sprint_tickets, done_this_sprint_tickets)
      
      
      CRs_done <- done_this_sprint_tickets %>% 
        filter(grepl("CR", Labels, fixed = TRUE) == TRUE)       # fixed = TRUE so that labels can have 'CRUD' in them but not be classified as CR
      
      CRs_to_do <- left_this_sprint_tickets %>% 
        filter(grepl("CR", Labels, fixed = TRUE) == TRUE)
      
      
      
      segmented_tickets <- list(done_before_this_sprint_tickets = done_before_this_sprint_tickets, 
                                done_this_sprint_tickets = done_this_sprint_tickets,
                                done_yesterday = done_yesterday,
                                contractor_done_yesterday = contractor_done_yesterday,
                                left_this_sprint_tickets = left_this_sprint_tickets,
                                whole_sprint_tickets = whole_sprint_tickets,
                                CRs_done = CRs_done, 
                                CRs_to_do = CRs_to_do)
      
      return(segmented_tickets)
    }
    
    segmented_tickets <- segment_tickets(start_date, end_date)
    
    
    
    
    sum_tickets <- function() {
      done_before_this_sprint_sum <- segmented_tickets$done_before_this_sprint_tickets %>% 
        summarise_points()
      
      done_this_sprint_sum <- segmented_tickets$done_this_sprint_tickets %>% 
        summarise_points()
      
      done_yesterday <- segmented_tickets$done_yesterday %>% 
        summarise_points()
      
      contractor_done_yesterday_sum <- segmented_tickets$contractor_done_yesterday %>% 
        summarise_points()
      
      left_this_sprint_sum <- segmented_tickets$left_this_sprint_tickets %>% 
        summarise_points()
      
      whole_sprint_sum <- segmented_tickets$whole_sprint_tickets %>% 
        summarise_points()
      
      CRs_done_sum <- segmented_tickets$CRs_done %>% 
        summarise_points()
      
      CRs_to_do_sum <- segmented_tickets$CRs_to_do %>% 
        summarise_points()
      
      tickets_sums <- list(done_before_this_sprint_sum = done_before_this_sprint_sum, 
                           done_this_sprint_sum = done_this_sprint_sum,
                           done_yesterday = done_yesterday,
                           contractor_done_yesterday_sum = contractor_done_yesterday_sum,
                           left_this_sprint_sum = left_this_sprint_sum,
                           whole_sprint_sum = whole_sprint_sum,
                           CRs_done_sum = CRs_done_sum, 
                           CRs_to_do_sum = CRs_to_do_sum)
      
      return(tickets_sums)
      
    }

    sums <- sum_tickets()
    
    
    points_remaining <- total_project_points$Total - 
      (sums$done_before_this_sprint_sum$Total + sums$done_this_sprint_sum$Total)
    
    percentage_used = ((points_remaining / total_project_points$Total) * 100) %>% round(., digits = 2)
    
    waste_this_sprint = sums$whole_sprint_sum$Unbilled
    
    
    make_morning_summary <- function() {
      
      all_totals <- list(
        `Yesterday's Date` = list(this_yesterday),
        `Project` = list(this_project),
        `Completed Yesterday` = list(sums$done_yesterday),
        `Contractor Completed Yesterday` = list(sums$contractor_done_yesterday_sum),
        `Total Project Points` = list(total_project_points),
        `Project Points Remaining` = list(points_remaining),
        `Percent Remaining` = list(percentage_used),
        `Sprint Estimate` = list(sums$whole_sprint_sum),
        `Completed This Sprint` = list(sums$done_this_sprint_sum),
        `Sprint Points Remaining` = list(sums$left_this_sprint_sum),
        `Waste This Sprint` = list(waste_this_sprint)
      ) %>% 
        as_tibble()
      
      all_totals_unnested <- unnest(all_totals, .sep = " ")
      
      
      all_totals_reordered <- all_totals_unnested %>% 
        select(
          `Yesterday's Date`, Project, 
          `Completed Yesterday Billed`, `Completed Yesterday Unbilled`,
          `Contractor Completed Yesterday Billed`,
          `Contractor Completed Yesterday Unbilled`,
          `Total Project Points Billed`, `Project Points Remaining`,
          `Percent Remaining`,
          `Sprint Estimate Billed`,
          `Completed This Sprint Billed`, `Sprint Points Remaining Billed`,
          # `Total Project Points Total`, `Project Points Remaining`, `Percent Remaining`,
          # `Completed Yesterday Total`, `Completed Yesterday Unbilled`, `Sprint Estimate Total`,
          # `Completed This Sprint Total`, `Sprint Points Remaining Total`,
          # `Waste This Sprint`, `Project Points Remaining`, `Percent Remaining`,
          everything()
          # -contains("Unbilled", ignore.case = FALSE)    # take out all columns with billed in the name
          # contains("Billed", ignore.case = FALSE),
          # ends_with("Total", ignore.case = FALSE)
        )
      
      return(all_totals_reordered)
    }
    
    morning_summary <- make_morning_summary()
    
    
    all_report <- rbind(all_report, morning_summary)
  }
  
  return(all_report)
}


# Transpose so that columns are projects
transpose_report <- function(all_report) {
  report <- all_report %>% select(-c(Project)) %>% t(.) %>% as.data.frame(.)
  names(report) <- all_report$Project
  
  report <- bind_cols(Metric = rownames(report), report)   # Keep rownames by column binding them to the output df
  return(report)
}

# ---- Actually create todays_date's report using the todays_date we got during ask_today()
todays_report <- make_report(todays_date) %>% transpose_report()

