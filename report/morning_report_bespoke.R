# July 1 through today, all billed points per project per day

source("./munge/combine.R")

per_day <- dat %>% 
  select(project, points, list_name, card_num, card_name, description, 
         labels, Member1, Member2, done,
         billed, wk_num, date) %>%
  filter(project == "BLUECHXP") %>% 
  rename(
    Project = project,
    Points = points,
    `List` = list_name,
    `Ticket Number` = card_num,
    `Ticket Name` = card_name,
    Description = description,
    Labels = labels,
    `Done?` = done,
    `Billed?` = billed,
    `Week of` = wk_num,
    `Date Completed` = date
  ) %>% 
  filter(`Date Completed` >= as_date("2017-08-03")) %>% 
  filter(
    `Done?` == "Yes"
  )

days <- unique(per_day$`Date Completed`)

out <- NULL

for(i in seq_along(days)) {
  this_day <- per_day %>%
    filter(`Date Completed` == days[i]) %>%
    summarise(
      Billed = sum(Points[.$`Billed?` == "Yes"], na.rm = TRUE),
      Unbilled = sum(Points[.$`Billed?` == "No"], na.rm = TRUE),
      `Total Points` = sum(Points, na.rm = TRUE)
    ) %>% 
    mutate(
      Date = days[i]
    ) %>% 
    select(
      Date, `Total Points`, everything()
    )

  out <- rbind(this_day, out)
}

per_day_sums <- per_day %>%
  group_by(`Date Completed`) %>%
  summarise(
    `Total Points` = sum(Points, na.rm = TRUE),
    Billed = sum(Points[`Billed?` == "Yes"], na.rm = TRUE),
    Unbilled = sum(Points[`Billed?` == "No"], na.rm = TRUE)
  )

total_sum <- per_day %>% 
  summarise(
    `Total Points` = sum(Points, na.rm = TRUE),
    Billed = sum(Points[.$`Billed?` == "Yes"], na.rm = TRUE),
    Unbilled = sum(Points[.$`Billed?` == "No"], na.rm = TRUE)
  )


# per_day_out <- spread(per_day_sums, key = Project, value = `Total Points Today`, fill = 0)


library(openxlsx)

wb <- createWorkbook(paste0("../../BLUECHXP 2.xlsx"))
addWorksheet(wb, sheetName = "Per Day")
writeData(wb, sheet="Per Day", out, keepNA = TRUE)
addStyle(wb, sheet = "Per Day", rows = 1, cols =
           1:ncol(per_day_out))
setColWidths(wb, sheet = "Per Day", cols = 1:ncol(per_day_sums), widths = "auto")


saveWorkbook(wb, paste0("../../BLUECHXP 2.xlsx"))



