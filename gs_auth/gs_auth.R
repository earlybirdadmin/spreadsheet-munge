library(httr)
library(googlesheets)

# Jenny's method (https://rawgit.com/jennybc/googlesheets/master/vignettes/managing-auth-tokens.html)
auth_token <- gs_auth()
saveRDS(auth_token, file = "googlesheets_token.rds")  # this sourced in in ./munge/points.R


# From http://metricbrew.com/using-service-account-with-googlesheets-r-package/
# g_oauth_token <- oauth_service_token(
#   oauth_endpoints("google"),
#   jsonlite::fromJSON("./gs_auth/Morning Reports Server Test-14cf6271e165.json"),
#   "https://www.googleapis.com/auth/userinfo.profile"
# )
# 
# gs_auth(token = g_oauth_token)  

# req <- GET("https://www.googleapis.com/oauth2/v1/userinfo", config(token = g_oauth_token))
# stop_for_status(req)
# content(req)


# Google developer console
# https://console.developers.google.com/apis/credentials?highlightClient=443849225538-ei0gsrm07ra4u1q9ub02aqajud4f384i.apps.googleusercontent.com&project=morning-reports-server-test


