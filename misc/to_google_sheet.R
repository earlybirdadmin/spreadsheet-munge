# to google sheet

source("./api/reporting.R")


# save google spreadsheet as an object 
master_rev_foo <- gs_title("master_rev_foo")

# get vector of worksheet names
ws_names <- master_rev_foo$ws[["ws_title"]]
ws_names



# -----  add data

# if we already have a worksheet name proj_name
# read what's in there and save it in sheet_current
# then get a diff of what's in sheet_current and what we want to put in there, want_tbl
# if there is a difference between what we have and what we want (i.e., nrow(diff) > 0)
# append that difference to the worksheet
sheet_exists <- function(g_workbook, want_tbl) {
  sheet_current <- gs_read(g_workbook, ws = proj_name)
  diff <- want_tbl[!want_tbl$eow_date %in% sheet_current$eow_date, ]
  
  if (nrow(diff) > 0) {
    g_workbook <- g_workbook %>% 
      gs_add_row(ws = proj_name, input = diff)
  }
}
# sheet_exists(master_rev_foo, g_out)

# if the sheet doesn't exist, create it and fill it with everything in want_tbl
new_sheet <- function(g_workbook, want_tbl) {
  g_workbook <- g_workbook %>% 
    gs_ws_new(proj_name) %>%
    gs_edit_cells(ws = proj_name, input = want_tbl)
}
# new_sheet(master_rev_foo, g_out)

# put them together 
add_data <- function(g_workbook, want_tbl) {
  if (proj_name %in% g_workbook$ws[["ws_title"]]) {
    sheet_exists(g_workbook, want_tbl)
  }
  else {
    new_sheet(g_workbook, want_tbl)
  }
}


# refresh and run the fn
master_rev_foo <- gs_title("master_rev_foo")
add_data(master_rev_foo, weekly_report)








# ------------- for easy testing/altering by hand: --------------
# # first time set up a sheet: add all data in done_by_week
# master_rev_foo <- master_rev_foo %>%
#   gs_ws_new(proj_name) %>%
#   gs_edit_cells(ws = proj_name, input = g_out[1:10, ])
# 
# 
# after that: add just the last row
# master_rev_foo <- master_rev_foo %>%
#   gs_add_row(ws = proj_name, input = g_out[2:30, ])

# ~ ~ ~ when in doubt, refresh with
# master_rev_foo <- gs_title("master_rev_foo")
# and check worksheets with 
# master_rev_foo$ws[["ws_title"]]
# can individual grab worksheet title with, e.g., foobar$ws[["ws_title"]][3]


