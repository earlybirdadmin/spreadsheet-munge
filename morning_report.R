# Morning Report

# ------ Metrics Calculated -----
# Total Points
# Points Completed
# Points Remaining
# Percentage Used
# Sprint Estimate
# Completed this sprint
# Sprint Points Remaining
# Waste this sprint


# ------ Assumptions Made -----
# Active counts as in the current scope of the sprint. Even if the sprint ends todays_date, active tickets are not counted as
# overflow from a previous sprint.
# Tickets in On Deck are not part of the current sprint.
#
# For finding yesterday's points: if todays_date is a Sunday or Monday, yesterday is this past Friday. Otherwise it's yesterday.
#
# If we don't have a sprint start date or it's in the past, set it to todays_date minus two weeks (i.e. the sprint ends todays_date).
# If we don't have a sprint end date, set it's the start date plus two weeks.
#
# If there's an Original Epics column, on the Trello, the total project points are pulled from there. (This is the ideal
# situation because this should be updated as unbilled work increases and CRs increase.) If there's no Original Epics column,
# the total point values are taken from the Production Log.

source("./munge/points.R")
source("./helpers/get_todays_date.R")
source("./helpers/look_for_contractors.R")

library(googlesheets)
library(lubridate)
library(openxlsx)

# ------------------------ Set Up ------------------------

# Either set today automatically or ask it
today_default()
# ask_today()



original_epic_column <- "Original Epics"
on_deck_columns <- c("Current Sprint", "Active")


# Set data to use. This will be what's used inside the morning_report() function. Put up here
# for convenience's sake in case this needs to change.
data_to_use <- dat


# --------------------- Run ----------------

# Read in the prod standups log
standups_log_all <- gs_title("Production Standups Log")
standups_log <- gs_read(ss = standups_log_all, ws = "Sprint Dates")


# Set types
standups_log$Project <- factor(standups_log$Project)
standups_log$`Sprint Start Date` <- as.Date(standups_log$`Sprint Start Date`)
standups_log$`Sprint End Date` <- as.Date(standups_log$`Sprint End Date`)
standups_log$`Total Project Points` <- as.numeric(standups_log$`Total Project Points`)


# generic point summary function for getting billed, unbilled and total from any df
summarise_points <- function(this_proj_df) {
  point_summary <- this_proj_df %>%
    summarise(
      Billed = sum(Points[this_proj_df$`Billed?` == "Yes"], na.rm = TRUE),
      Unbilled = sum(Points[this_proj_df$`Billed?` == "No"], na.rm = TRUE),
      Total = sum(Points, na.rm = TRUE)
    )

  return(point_summary)
}


get_sprint_end_dates <- function() {
  for (row in 1:nrow(standups_log)) {

    # if we don't have a sprint start date, set it to todays_date minus two weeks (i.e. sprint ending todays_date)
    if (is.na(standups_log$`Sprint Start Date`[row])) {
      standups_log$`Sprint Start Date`[row] <- Sys.Date() - 14
    }

    # if we don't have a sprint end date or it's in the past, set it to the start date plus two weeks
    if (is.na(standups_log$`Sprint End Date`[row]) | standups_log$`Sprint End Date`[row] < Sys.Date()) {
      standups_log$`Sprint End Date`[row] <- as.Date(standups_log$`Sprint Start Date`[row] + 14)
    }
  }
  return(standups_log)
}

standups_log <- get_sprint_end_dates()



# Function for finding what we're calling "Yesterday's Date"
# If it's a Sunday, or Monday, yesterday is Friday. Else it's yesterday.
find_yesterday <- function(d) {
  d <- as.Date(d)
  y_day <- d - 1
  if (wday(d) == 1) {         # if it's sunday
    y_day <- d - 2
  } else if (wday(d) == 2) {     # if it's monday
    y_day <- d - 3
  }
  return(y_day)
}


# Run the main function
source("./report/make_morning_report.R")
todays_report

# ----------------- Add yesterday's data ----------------------

# name the sheet with todays_date's date
# sheet_name <- paste(months(todays_date), day(todays_date), sep = " ")


# if the sheet doesn't exist, add it and write to it. otherwise, overwrite it.
add_data <- function(g_workbook, want_tbl, d) {
  sheet_name <- paste(months(d), day(d), sep = " ")

  if (sheet_name %in% g_workbook$ws$ws_title) {
    g_workbook <- g_workbook %>%
      gs_edit_cells(ws = sheet_name, input = want_tbl)
  } else {
    g_workbook <- g_workbook %>%
      gs_ws_new(sheet_name, input = want_tbl)
  }

}


# refresh spreadsheets
morning_report <- gs_title("Morning Reports")
# morning_report_test <- gs_title("Morning Reports Test")
standups_log_all <- gs_title("Production Standups Log")
# standups_log <- gs_url("https://docs.google.com/spreadsheets/d/1nVxSztr7iKyxJFiK7jXDeiJQFEO38EtU0JE4BTZvlQs/edit#gid=1065261312")


# add the data
add_data(morning_report, todays_report, todays_date)
# add_data(morning_report_test, todays_report, todays_date)
add_data(standups_log, todays_report, todays_date)


