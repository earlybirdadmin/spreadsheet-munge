
# get df of only EB team members: doesn't include contractors
eb_team <- get_team_members(team_id, token = token)

# change to the names that appears on her hours sheet
eb_team$fullName[which(eb_team$fullName=="Rachel")] <- "Rachel Schneebaum"
eb_team$fullName[which(eb_team$fullName=="Kris Kroski")] <- "Kristopher Kroski"
eb_team$fullName[which(eb_team$fullName=="Brad Siefert")] <- "Bradley Siefert"

look_for_contractors <- function(d) {
  d$contract <- "x"
  for (i in 1:nrow(d)) {
    if (is.na(d$Member1[i])) {
      d$contract[i] <- "No"
    } else if (
      (!(is.na(d$Member1[i])) & !(d$Member1[i] %in% eb_team$fullName)) | 
      (!(is.na(d$Member2[i])) & !(d$Member2[i] %in% eb_team$fullName)) |
      (!(is.na(d$Member3[i])) & !(d$Member3[i] %in% eb_team$fullName)) |
      (!(is.na(d$Member4[i])) & !(d$Member4[i] %in% eb_team$fullName))) {
      d$contract[i] <- "Yes"
    } else {
      d$contract[i] <- "No"
    }
  }
  d
}

