
library(shiny)
library(Hmisc)
library(DT)
library(shinythemes)
# devtools::install_github("aedobbyn/dobtools")
# library(dobtools)

# Sys.setenv(TZ='UTC' + 9.5)

summarise_points <- function (df, lowercase = TRUE, ...) {
  if (lowercase == TRUE) {
    point_summary <- df %>% summarise(Billed = sum(points[billed == "Yes"], na.rm = TRUE), 
                                      Unbilled = sum(points[billed == "No"], na.rm = TRUE), 
                                      Total = sum(points, na.rm = TRUE))
  }
  else {
    point_summary <- df %>% summarise(Billed = sum(Points[`Billed?` == "Yes"], na.rm = TRUE), 
                                      Unbilled = sum(Points[`Billed?` == "No"], na.rm = TRUE), 
                                      Total = sum(Points, na.rm = TRUE))
  }
  return(point_summary)
}


source("./kroski_api.R")

this_week <- week(Sys.Date())
this_year <- year(Sys.Date())

format_cols <- function(df) {
  for (i in seq_along(names(df))) {
    if (is.numeric(df[, i])) {
      df[, i] <- df[, i] %>% unlist() %>%  round(digits = 1) }
  }
  names(df) <- capitalize(names(df))
  return(df)
}



ui <- fluidPage(
  
  theme = shinytheme("spacelab"),
   
   titlePanel("How many points has Kroski done this week?"),
  
  p("Week", this_week),
  
  br(), br(),
   
   DT::dataTableOutput("kroski_points")
   
   # actionButton("refresh", "Refresh")
)




server <- function(input, output) {
  
  kroski_done_tickets <- reactive({ dat %>% 
    filter(
      Member1 == "Kristopher Kroski" &
        done == "Yes" &
        week(date) == this_week &
        year(date) == this_year
    ) 
  })
  
  kroski_done_tickets_grouped <- reactive({ kroski_done_tickets() %>%
    group_by(project, date)})
  
  kroski_points <- reactive({ 
    summarise_points(kroski_done_tickets_grouped(), lowercase=TRUE) %>% 
                       ungroup() %>% 
                       mutate(project = as.character(project))   # so we don't coerce factor to character
  })
  
  kroski_total <- reactive({ kroski_done_tickets() %>% 
    summarise_points(lowercase=TRUE) %>% 
    mutate(
      project = "Total" %>% as.character(), 
      date = Sys.Date()
    ) %>% 
    select(date, project, everything()) 
  })
  
  kroski_in <- reactive({ bind_rows(kroski_points(), kroski_total()) }) 

  kroski_out <- reactive({ kroski_in() %>% format_cols() })
   
   output$kroski_points <- DT::renderDataTable({
     # input$refresh
     
     kroski_out()
     
   }, server = FALSE, options = list(dom = 'pt'))
}


shinyApp(ui = ui, server = server)

